package pl.technologies.transition.oop;

import java.util.Random;
import java.util.UUID;

public class ObjectProblemSolver {

	public static void main(String[] args) {

		Computer[] computers = getComputers();
		int sum = 0;

		// Problem: Print a sum of prices of all the computers
		// Here put your code:

		
		
		
		System.out.println("Price of all computers: " + sum);

	}

	static Computer[] getComputers() {
		Computer[] computers = new Computer[4];
		Random random = new Random();
		for (Computer computer : computers) {
			computer.setName("Computer-" + UUID.randomUUID().toString().substring(0, 8));
			computer.setPrice(random.nextInt(3000));
		}
		return computers;
	}

}

class Computer {

	private String name;
	private int price;

	Computer(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return name + " " + price;
	}

}
