package pl.technologies.transition.oop.customcollections;


public class MyCustomStackTester {

    public static void main(String[] args) {
        MyCustomStackTester stackTester = new MyCustomStackTester();
        stackTester.testNonOOPSolution();
        stackTester.testOOPSolution();

    }

    private void testNonOOPSolution() {

        int stackTipPointer = 0;
        int[] mystack = new int[10];

        // dodajemy do stosu
        int firstNumber = 6;
        mystack[stackTipPointer++] = firstNumber;

        int secondNumber = 4;
        mystack[stackTipPointer++] = secondNumber;

        // pobieramy ze stosu
        int value = mystack[--stackTipPointer];
        System.out.println("value = " + value);

        value = mystack[--stackTipPointer];
        System.out.println("value = " + value);

        // problemy - co jeśli dodamy zbyt wiele elementów?
        // co jeśli stos jest pusty?

    }

    private void testOOPSolution() {
        CustomStack customStack = new MyCustomStack();

        customStack.push(10);
        customStack.push(30);

        System.out.println("value: " + customStack.pop());
        System.out.println("value: " + customStack.pop());

    }


}
