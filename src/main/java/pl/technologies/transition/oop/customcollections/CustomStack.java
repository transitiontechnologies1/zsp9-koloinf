package pl.technologies.transition.oop.customcollections;

public interface CustomStack {

    void push(int value);

    int pop();

    boolean isEmpty();

}
