package pl.technologies.transition.oop.customcollections;


public class MyCustomStack implements CustomStack{
    public void push(int value) {
        throw new RuntimeException("Not implemented");
    }

    public int pop() {
        throw new RuntimeException("Not implemented");
    }

    public boolean isEmpty() {
        throw new RuntimeException("Not implemented");
    }
}
