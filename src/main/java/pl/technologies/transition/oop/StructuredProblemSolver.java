package pl.technologies.transition.oop;

import java.util.Random;
import java.util.UUID;

public class StructuredProblemSolver {

	public static void main(String[] args) {

		String[][] computers = getComputers();
		int sum = 0;

		// Problem: Print a sum of prices of all the computers
		// To convert from String to int, use Integer.valueOf(String), e.g.
		// int price = Integer.valueOf("500");
		// Here put your code:
		
		
		
		
		System.out.println("Price of all computers: " + sum);

	}

	static String[][] getComputers() {
		String[][] computers = new String[4][2];
		Random random = new Random();
		for (String[] computer : computers) {
			computer[0] = "Computer-" + UUID.randomUUID().toString().substring(0, 8);
			computer[1] = String.valueOf(random.nextInt(3000));
		}
		return computers;
	}

}
