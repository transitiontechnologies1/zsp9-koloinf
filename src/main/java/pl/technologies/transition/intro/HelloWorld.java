package pl.technologies.transition.intro;

public class HelloWorld {

	public static void main(String[] args) {

		System.out.println("Hello world");
		
		// ==
		
		String helloWorld = "Hello world";
		System.out.println(helloWorld);

		// or even
		
		String hello = "Hello";
		String world = "world";
		System.out.println(hello + " " + world + "!");
		
	}

}
