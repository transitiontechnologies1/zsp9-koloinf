package pl.technologies.transition.intro.array;

public class ArrayExample {

	public static void main(String[] args) {
		
		int[] a = { 1, 2, 3 };
		// ==
		// int[] a = new int[] { 1, 2, 3 };
		
		System.out.println("Array length: " + a.length);
		
		System.out.println("Array items");
		System.out.println("Index 0: " + a[0]);
		System.out.println("Index 1: " + a[1]);
		System.out.println("Index 2: " + a[2]);
		
		a[0] = 8;
		System.out.println("Index 0: " + a[0]);
		
		// would it work?
		a[3] = 5;
		System.out.println("Index 3: " + a[3]);
		
		
		boolean[] b = new boolean[5];
		System.out.println("B length: " + b.length);
		
	}
	
	
}
