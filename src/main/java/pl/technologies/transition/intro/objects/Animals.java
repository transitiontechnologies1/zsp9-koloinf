package pl.technologies.transition.intro.objects;

public abstract class Animals {

	String type;
	double age;

	public Animals() {

	}

	public Animals(String type, double age) {
		this.type = type;
		this.age = age;
	}

	public abstract void makeNoise();
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Animal type: " + type + " and age: " + age;
	}
}
