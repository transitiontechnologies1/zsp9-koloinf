package pl.technologies.transition.intro.objects;

public class Zoo {

	public static void main(String[] args) {

		Animals cat = new Cat();
		Dog dog = new Dog();

		printAnimal(cat);
		System.out.println("Cat gives voice:");
		cat.makeNoise();

		System.out.println("================");

		printAnimal(dog);
		System.out.println("Dog gives voice:");
		dog.makeNoise();

		// 1. Why type == null and age == 0.0?
		// 2. Why dog is not yelling?

		// 3. Note, that despite the fact, that cat is Animal, it make cat
		// noises. It's calles polymorphism.

		System.out.println("================");
		Animals[] group = new Animals[2];
		group[0] = cat;
		group[1] = dog;

		System.out.println("Cat gives voice:");
		group[0].makeNoise();

		group[1].setAge(5);
		System.out.println("Dog age: " + dog.getAge());

	}

	// 4. Note, that despite the fact, that the param needs Animal, we can pass
	// here Cat or Dog
	static void printAnimal(Animals animal) {
		System.out.println(animal);
	}

}
