package pl.technologies.transition.intro.objects.modifiers;

public class NonAccessModifiers {

	public String normalString = "abc";
	public static String staticString = "static";
	public final String finalString = "final";
	public final static String CONSTANS = "constans";
	
}
