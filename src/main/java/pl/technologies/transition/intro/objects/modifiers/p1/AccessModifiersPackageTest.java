package pl.technologies.transition.intro.objects.modifiers.p1;

public class AccessModifiersPackageTest {

	void printVariables() {
		
		AccessModifiers modifiers = new AccessModifiers();
		System.out.println("a: " + modifiers.a);
		System.out.println("b: " + modifiers.b);
		System.out.println("c: " + modifiers.c);
		// System.out.println("d: " + modifiers.d);
		
	}
	
}
