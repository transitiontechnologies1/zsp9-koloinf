package pl.technologies.transition.intro.objects.modifiers;

public class NonAccessModifiersTest {

	public static void main(String[] args) {
		
		NonAccessModifiers modifiers = new NonAccessModifiers();
		System.out.println("Normal String: " + modifiers.normalString);
		
		modifiers.normalString = "cba";
		System.out.println("Normal String changed: " + modifiers.normalString);
		
		System.out.println("Static String: " + NonAccessModifiers.staticString);
		
		System.out.println("Final String: " + modifiers.finalString);
		// modifiers.finalString = "changed!";
		
		System.out.println("Constans: " + NonAccessModifiers.CONSTANS);
		
	}
	
}
