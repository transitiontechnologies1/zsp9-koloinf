package pl.technologies.transition.intro.basic_types;

public class StringExample {

	public static void main(String[] args) {
		
		String a = "This ";
		String b = "is ";
		String c = "String!";
		String concatenated = a + b + c;
		System.out.println(concatenated);
		
		// note a various number of String methods
		// go ahead, explore!
		int length = concatenated.length();
		System.out.println("Length of the string: " + length);
		
		int indexOfExclamationMark = concatenated.indexOf("!");
		System.out.println("Index of exclamation mark: " + indexOfExclamationMark);
		
		String substring = concatenated.substring(0, 4);
		System.out.println("First four chars: " + substring);
		
		int index = 9;
		char charAt = concatenated.charAt(index);
		System.out.println("Char at " + index + ": " + charAt);
		
	}
	
}
