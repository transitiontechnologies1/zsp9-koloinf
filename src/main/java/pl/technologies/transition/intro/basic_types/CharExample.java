package pl.technologies.transition.intro.basic_types;

public class CharExample {

	public static void main(String[] args) {
		
		// multiple declarations
		char a, b;
		
		a = 'a';
		b = 97;
		System.out.println("Char a is: " + a + ", char b is: " + b);
		
		char c = (char) (a + b);
		System.out.println("Char c is: " + c);
		
		// char is single 16-bit Unicode character (min value = 0, max value = 65535)
		
	}
	
}
