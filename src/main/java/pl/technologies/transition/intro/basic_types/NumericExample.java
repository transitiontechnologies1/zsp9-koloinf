package pl.technologies.transition.intro.basic_types;

public class NumericExample {

	public static void main(String[] args) {

		byte a = 5;
		System.out.println("Byte: " + a);
		
		int b = a;
		System.out.println("Integer: " + b);

		// why casting?
		short c = (short) b;
		System.out.println("Short from integer: " + c);
		
		// note the L on the end of literal
		long d = 9223372036854775807L;
		byte e = (byte) d;
		System.out.println("Byte from long: " + e);
	
		// note the f on the end of literal
		float f = 1.1f;
		double g = 123.45;
		System.out.println("Float: " + f + ", double: " + g);
		
		
	}

}
