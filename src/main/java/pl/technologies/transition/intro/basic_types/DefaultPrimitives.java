package pl.technologies.transition.intro.basic_types;

public class DefaultPrimitives {

	public static void main(String[] args) {

		// not initialized fields
		EmptyFields empty = new EmptyFields();
		System.out.println(empty);

	}

}

class EmptyFields {

	byte a;
	short b;
	int c;
	long d;
	float e;
	double f;
	char g;
	String h;
	boolean i;

	@Override
	public String toString() {
		return "Byte: " + a + ",\nshort: " + b + ",\nint: " + c + ",\nlong: " + d + ",\nfloat: " + e + ",\ndouble: " + f
				+ ",\nchar: " + g + ",\nString: " + h + ",\nboolean: " + i;
	}

}
