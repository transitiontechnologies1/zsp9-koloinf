package pl.technologies.transition.intro.basic_types;

public class OperationExample {

	public static void main(String[] args) {

		int a = 543;
		int b = 123;
		
		System.out.println("a + b: " + (a + b));
		System.out.println("a - b: " + (a - b));
		System.out.println("a * b: " + (a * b));
		System.out.println("a / b: " + (a / b));
		System.out.println("a > b: " + (a > b));
		System.out.println("a == b: " + (a == b));
		
		boolean c = true;
		boolean d = false;
		System.out.println("c == d: " + (c == d));
		System.out.println("c && d: " + (c && d));
		System.out.println("c || d: " + (c || d));
		
		
		
	}

}
