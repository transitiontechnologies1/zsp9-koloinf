package pl.technologies.transition.intro.loops;

public class IfExample {

	public static void main(String[] args) {

		int a = 1;
		if (a == 1) {
			System.out.println("a is 1!");
		} else if (a == 2) {
			System.out.println("a is 2!");
		} else if (a == 3) {
			System.out.println("a is 3!");
		} else {
			System.out.println("a is not 1, 2 or 3!");
		}

	}

}
