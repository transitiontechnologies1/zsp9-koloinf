package pl.technologies.transition.intro.loops;

public class DoWhileExample {

	public static void main(String[] args) {

		char a[] = new char[0];
		int index = 0;

		System.out.println("While loop with empty array:");
		while (index < a.length) {
			System.out.println(a[index]);
			index++;
		}

		System.out.println("Do while loop with empty array:");
		index = 0;
		do {
			System.out.println(a[index]);
			index++;
		} while (index < a.length);

	}

}
