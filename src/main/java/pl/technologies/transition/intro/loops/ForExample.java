package pl.technologies.transition.intro.loops;

public class ForExample {

	public static void main(String[] args) {
		
		char a[] = { 'a', 'b', 'c', 'd' };
		
		for(int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
		
		
	}
	
}
