package pl.technologies.transition.intro.loops;

public class ForEachExample {

	public static void main(String[] args) {
		
		char a[] = { 'a', 'b', 'c', 'd' };
		
		for(char ch : a) {
			System.out.println(ch);
		}
		
	}
}
