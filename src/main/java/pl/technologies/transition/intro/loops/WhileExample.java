package pl.technologies.transition.intro.loops;

public class WhileExample {

	public static void main(String[] args) {
		
		char a[] = { 'a', 'b', 'c', 'd' };
		
		int index = 0;
		while(index < a.length) {
			System.out.println(a[index]);
			index++;
		}
		
	}
	
}
