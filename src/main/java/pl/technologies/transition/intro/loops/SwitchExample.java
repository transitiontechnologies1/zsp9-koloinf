package pl.technologies.transition.intro.loops;

public class SwitchExample {

	public static void main(String[] args) {

		// if a == 1, two sentences are printed - why?
		int a = 1;
		
		switch (a) {
		case 1:
			System.out.println("a is 1!");
		case 2:
			System.out.println("a is 2!");
			break;
		case 3:
			System.out.println("a is 3!");
			break;
		default:
			System.out.println("a is not 1, 2 or 3!");
		}

	}

}
