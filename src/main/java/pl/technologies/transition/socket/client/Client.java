package pl.technologies.transition.socket.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main(String[] args) throws UnknownHostException, IOException {

		try (BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.println("Wpisz IP hosta: ");
			String hostIP = inFromUser.readLine();
			try (Socket socket = new Socket(hostIP, 6789);
					DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
					BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

				while (true) {
					System.out.println("Message to host:");
					String messageToServer = inFromUser.readLine();
					outToServer.writeBytes(messageToServer + "\n");
					String messageFromServer = inFromServer.readLine();

					System.out.println("Host said: " + messageFromServer);
				}

			}

		}
	}
}