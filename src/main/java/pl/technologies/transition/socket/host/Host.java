package pl.technologies.transition.socket.host;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Host {

	public static void main(String[] args) throws IOException {

		System.out.println(InetAddress.getLocalHost().getHostAddress());
		System.out.println("Waiting for connection...");

		try (BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
				ServerSocket serverSocket = new ServerSocket(6789);
				Socket socket = serverSocket.accept();
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				DataOutputStream outToClient = new DataOutputStream(socket.getOutputStream())) {

			while (true) {
				String messageFromClient = inFromClient.readLine();
				System.out.println("Client said: " + messageFromClient);

				String messageToClient = inFromUser.readLine();
				outToClient.writeBytes(messageToClient + "\n");
			}

		}

	}

}
