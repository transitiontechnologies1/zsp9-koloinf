package pl.technologies.transition.warzone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CardDeck {

	private List<Card> cards = new ArrayList<Card>();
	
	public List<Card> getCards() {
		return cards;
	}

	public CardDeck() {
		
		List<String> suits = Arrays.asList(
				"Kier", 
				"Karo", 
				"Trefl",
				"Pik");
		
		for (int rank = 2; rank <= 14; rank ++) {
			for (String suit : suits) {
				Card card = new Card(rank, suit);
				cards.add(card);
			}
		}
		
	}

	public void shuffle() {

		Random j = new Random();
		for(int i=0;i < cards.size();i++){
			int x = j.nextInt(cards.size()-1);
			Collections.swap(cards, i, x);
		}
	}
}
