package pl.technologies.transition.warzone;

public class Card {
	private int rank;
	
	private String suit;

	public int getRank() {
		return rank;
	}

	public String getSuit() {
		return suit;
	}
	
	public String getName(){
		return rank + " " + suit;
	}

	public Card(int rank, String suit) {
		super();
		this.rank = rank;
		this.suit = suit;
	}
	
	
	
}
