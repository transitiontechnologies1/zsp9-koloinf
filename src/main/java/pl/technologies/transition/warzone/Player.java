package pl.technologies.transition.warzone;

public abstract class Player {

	protected PlayerCards playerCards = new PlayerCards();
	protected OnHandCards onHandCards;

	public abstract Card getTopCard();
	public abstract String getNick();
	
	protected int points = 0;
	
	public void getCard(Card card) {
		playerCards.addCard(card);
	}

	public boolean hasMoreCards() {
		return playerCards.hasMoreCards();
	}

	public int getPoints() {
		return points;
	}

	public void addPoint() {
		this.points++;
	}
	
	public void upkeepHandCards() {
		onHandCards = new OnHandCards(playerCards);
	}
	
	public abstract Card chooseCard();
	
	@Override
	public String toString() {
		return getNick();
	}
}
