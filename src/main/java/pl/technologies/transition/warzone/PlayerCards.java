package pl.technologies.transition.warzone;

import java.util.ArrayList;
import java.util.List;

public class PlayerCards {

	private List<Card> cards = new ArrayList<Card>();

	public PlayerCards(){
		
	}
	
	public PlayerCards(List<Card> cards) {
		super();
		this.cards = cards;
	}
	
	public Card getFirstCard() {
		Card firstCard = cards.get(0);
		cards.remove(firstCard);
		
		return firstCard;
	}
	
	public void addCard(Card card) {
		cards.add(card);
	}

	public boolean hasMoreCards() {
		return !cards.isEmpty();
	}

	public boolean isEmpty() {
		return cards.isEmpty();
	}
	
}
