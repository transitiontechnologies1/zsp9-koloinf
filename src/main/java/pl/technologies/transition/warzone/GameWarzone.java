package pl.technologies.transition.warzone;

import java.util.List;

public class GameWarzone {
	
	private Player player1;
	private Player player2;

	public static void main(String[] args) {
		GameWarzone gameWarzone = new GameWarzone();
		gameWarzone.playGame();
	}
	
	void playGame() {
		initializePlayers();
		
		dealCards();
		
		while (playersHavecards()) {
			upkeep();
			playGameRound();
		}
		
		getTotalScore();	
	}

	private boolean playersHavecards() {
		return player1.hasMoreCards() && player2.hasMoreCards();
	}

	private void initializePlayers() {
		player1 = new HumanPlayer("Hardkorowy Koksu");
		player2 = new HumanPlayer("Mariusz Pudzianowski");
	}

	private void upkeep() {
		player1.upkeepHandCards();
		player2.upkeepHandCards();
	}

	private void playGameRound() {
		Card p1Card = player1.chooseCard();
		Card p2Card = player2.chooseCard();
		System.out.println(player1.getNick() + " ma kart� " + p1Card.getName() + ", "
				+ player2.getNick() + " ma kart� " + p2Card.getName() + " ");
		if(p1Card.getRank() > p2Card.getRank()){
			System.out.println("> " + player1.getNick() + " ma lepsz� kart�!\n");
			player1.addPoint();
		} else if(p1Card.getRank() < p2Card.getRank()){
			System.out.println("> " + player2.getNick() + " ma lepsz� kart�!\n");
			player2.addPoint();
		} else if(p1Card.getRank() == p2Card.getRank()){
			System.out.println("*** Taki sam poziom kart! ***");
			System.out.println("*** Punkty nie rozdane dla nikogo. ***\n");
		}
	}

	private void getTotalScore() {
		System.out.println("\nGra sko�czona!\nIlo�� punkt�w:\n" + player1.getNick() + " = " + player1.getPoints()
			+ ",\n" + player2.getNick() + " = " + player2.getPoints() + ".");
		if(player1.getPoints() > player2.getPoints()){
			System.out.println("\nWygrywa gracz o pseudonimie " + player1.getNick() + "!");
		} else if(player2.getPoints() > player1.getPoints()){
			System.out.println("\nWygrywa gracz o pseudonimie " + player2.getNick() + "!");
		} else if(player1.getPoints() == player2.getPoints()){
			System.out.println("\nREMIS!"); 	//Dop�ki karty nie s� dorzucane wygrywaj�cemu graczowi, mo�liwy.
		}
	}

	private void dealCards() {
		CardDeck cardDeck = new CardDeck();
		cardDeck.shuffle();
		
		List<Card> cards = cardDeck.getCards();
		for (int i = 0; i < cards.size(); i++) {
			Card card = cards.get(i);
			if (i % 2 == 0) {
				player1.getCard(card);
			} else {
				player2.getCard(card);
			}
		}
		
	}
}
