package pl.technologies.transition.warzone;

import java.util.ArrayList;
import java.util.List;

public class OnHandCards {

	private static final int HAND_SIZE = 4;
	
	private List<Card> cards = new ArrayList<Card>();
	private PlayerCards playerCards;
	
	public List<Card> getOnHand(){
		return cards;
	}
	
	public OnHandCards(PlayerCards playerCards){
		this.playerCards = playerCards;
		initializeHandCards();
	}

	private void initializeHandCards() {
		for (int i = 0; i < HAND_SIZE; i++) {
			if (playerCards.isEmpty()) {
				break;
			}
			cards.add(playerCards.getFirstCard());
		}
	}
}
