package pl.technologies.transition.warzone;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class HumanPlayer extends Player {

	private String nick;
	
	public String getNick() {
		return nick;
	}

	public HumanPlayer(String nick) {
		super();
		this.nick = nick;
	}
	
	@Override
	public Card getTopCard() {
		return playerCards.getFirstCard();
	}
	
	public Card chooseCard(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("PLAYER: " + toString());
		System.out.println("You have the following cards on hand:");
		List<Card> onHand = onHandCards.getOnHand();
		
		int index = 1;
		for (Card card : onHand) {
			System.out.println(index++ + " - " + card.getName());
		}
		
		System.out.println("choice: ");
//		double nextDouble = scanner.nextDouble();
		int choice = (int) scanner.nextDouble();
//		scanner.close();
		
//		return null;
		return onHand.get(choice -1);
	}
	
}
