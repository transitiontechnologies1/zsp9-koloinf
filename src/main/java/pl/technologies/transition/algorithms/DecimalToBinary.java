package pl.technologies.transition.algorithms;

import java.util.Scanner;

public class DecimalToBinary {

	// Zamie� liczb� ca�kowit� na binarn� (znajd� b��d w poni�szym programie i
	// go napraw)

	public static void main(String[] args) {

		int number;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj libcz� ca�kowit�, kt�ra zostanie zamieniona na liczb� binarn�:");
		number = scanner.nextInt();
		scanner.close();

		System.out.println(convertToBinaryFormat(number));
		
	}

	static String convertToBinaryFormat(int number) {
		String binary = "";
		while (number > 0) {
			binary += number % 2;
			number = number / 2;
		}

		return binary;
	}

}
