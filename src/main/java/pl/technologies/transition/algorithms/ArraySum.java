package pl.technologies.transition.algorithms;

public class ArraySum {
	// Policz sume podanych liczb
	public static void main(String[] args) {
		int[] array = { 1, 44, 23, 51, 54, 136, 124 };
		
		// oblicz przy wykorzystaniu p�tli for()
		long suma=0;
		for(int i=0;i<7;i++){
			suma += array[i];
		}

		// oblicz przy wykorzystaniu p�tli 'foreach'
		long suma2=0;
		for(int a : array){
			suma2 += a;
		}
		
		// wypisz wynik
		System.out.println("Suma: " + suma);
		System.out.println("Suma: " + suma2);
	}
	
}
