package pl.technologies.transition.algorithms;

public class RecursiveFactorial {

	// Dla danej liczby ca�kowitej 'N' policz N! (silnia) wiedz�c, �e 0!=!=1,
	// za� dla N>1 mamy N! = 1 * 2 * ... * N (napisz funkcj� rekursywn�)
	public static void main(String[] args) {
		System.out.println(factorial(5));
	}

	static long factorial(int n) {
		return 1;
	}
}
