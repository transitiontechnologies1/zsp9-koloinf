package pl.technologies.transition.algorithms;

public class Fibonacci {

	/******************************************************************************
	 * 	Znajd� liczb� Fibonacciego wiedz�c �e:
	 *  Fk = 1  dla k=1,2
	 *  Fk-1 + Fk-2  dla k>2
	 *  
	 *  Do rozwi�zania problemu u�yj tablic
	 *  
	 *  Compilation:  javac Fibonacci.java
	 *  Execution:    java Fibonacci N
	 *
	 *  Computes and prints the first N Fibonacci numbers.
	 *
	 *  WARNING:  this program is spectacularly inefficient and is meant
	 *            to illustrate a performance bug, e.g., set N = 45.
	 *
	 *   % java Fibonacci 7
	 *   1: 1
	 *   2: 1
	 *   3: 2
	 *   4: 3
	 *   5: 5
	 *   6: 8
	 *   7: 13
	 *
	 ******************************************************************************/
	
	public static void main(String[] args) {

	}

}
