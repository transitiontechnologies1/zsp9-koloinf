package pl.technologies.transition.algorithms;

public class Homework {

	/**
	 * 
	 * 'Luka binarna' wewn�trz naturalnej liczby N jest najd�u�sz� sekwencj�
	 * kolejnych zer, kt�re otoczone s� z dw�ch stron '1' w binarnej
	 * reprezentacji liczby N.
	 * 
	 * Dla przyk�adu, liczba 9 ma reprezentacje 1001 i zawiera 'luke binarn�' o
	 * d�ugo�ci 2. Liczba 529 ma reprezentacje 1000010001 i zawiera dwie 'luki
	 * binarne': pierwsza o d�ugo�ci 4 i drug� o d�ugo�ci 3. Liczba 20: 10100
	 * zawiera jedn� 'luk� binarn�" o d�ugo�ci 1. Liczba 15: 1111 nie posiada
	 * 'luki binarnej'.
	 * 
	 * Napisz funkcje:
	 * 
	 * class Homework { public int solution(int N); } kt�ra przyjmuje liczb�
	 * naturaln� integer N i zwraca d�ugo�� najd�u�szej 'luki binarnej'. Funkcja
	 * powinna zwraca� 0 kiedy N nie posiada 'luki binarnej'.
	 * 
	 * Dla przyk�adu, N = 1041 - funkcja powinna zwr�ci� 5, poniewa� N posiada
	 * reprezenatcje binarn� 10000010001 i to jest najd�u�sza 'binarna luka' o
	 * d�ugo�ci 5.
	 * 
	 * Podsumowuj�c:
	 * 
	 * N jest integer z zakresu [1..2,147,483,647]. Z�o�ono��:
	 * 
	 * zak�adana najgorsza z�o�ono�� obliczeniowa (time complexity) O(log(N)) 
	 * zak�adana najgorsza z�o�ono�� miejsca (space complexity) wynosi O(1)
	 * 
	 */

	public static void main(String[] args) {

	}

}
