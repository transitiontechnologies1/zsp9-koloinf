package pl.technologies.transition.algorithms;

import java.util.Scanner;

public class QuadraticEquation {

	// Rozwi�� r�wnanie kwadratowe postaci ax^2+bx+c = 0

	public static void main(String[] args) {

		double a, b, c;
		
		Scanner scanner = new Scanner(System.in);

		System.out.println("ax^2+bx+c = 0\nWprowad�:\na=");
		a = scanner.nextDouble();
		System.out.println("b=");
		b = scanner.nextDouble();
		System.out.println("c=");
		c = scanner.nextDouble();
		
		scanner.close();

		System.out.format("Rozwi�zuje r�wnanie %.2fx^2+%.2fx+%.2f = 0....", a, b, c);
		System.out.format("\nRozwi�zuje r�wnanie %.2fx^2+%.2fx+%.2f = 0....", a, b, c);

		double delta=0, x1=0, x2=0;
		delta = Math.pow(b, 2) - (4*a*c);
		
		if(delta > 0){
			x1 = (-b-Math.sqrt(delta))/2*a;
			x2 = (-b+Math.sqrt(delta))/2*a;
			System.out.println("\nDelta > 0, x1 = " + x1 + ", x2 = " + x2);
		} else if(delta == 0){
			 x1 = -b/(2*a);
			 System.out.println("\nDelta == 0, x1 = " + x1);
			} else System.out.println("\nDelta < 0. Nie ma pierwiastkow");
	}

}
